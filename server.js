/* jslint node: true */
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const routes = express()
const mongoose = require('mongoose')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const passport = require('passport');

//reads in configuration from a .env file
require('dotenv').config()

// Require configuration file defined in app/config.js
const config = require('./app/config/config');

const allowedURL = config.ALLOWED_URL;
const port = config.APP_PORT;
const dbPort = config.DB_PORT;
const dbUrl = config.DB_URL;
const dbUser = config.DB_USER;
const dbPwd = config.DB_PWD;
const dbCollection = config.DB_COLLECTION;

// Connect to database
//TODO add authentication on ${dbUser}:${dbPwd}@
mongoose.connect(`mongodb://${dbUrl}:${dbPort}/${dbCollection}`, {useUnifiedTopology:true, useNewUrlParser:true, useCreateIndex:true})
       .then(_ => console.log('Connected Successfully to MongoDB'))
       .catch(err => console.error(err));

// Sends static files  from the public path directory
routes.use(express.static(path.join(__dirname, '/public')));

// Use morgan to log request in dev mode
routes.use(morgan('dev'));
routes.use(bodyParser.json());
//extended ?
routes.use(bodyParser.urlencoded({extended: true}));

//sets the required variables from Environment Variables.
// Listen on port defined in config file
routes.listen(port, err => {
    if(err) console.error(err);
    //TODO add multilanguage
    console.log(`Listening for Requests on port: ${port}`);
});

//initializes the passport configuration.
routes.use(passport.initialize());
require('./app/config/passport-config')

//imports our configuration file which holds our verification callbacks and things like the secret for signing.
routes.use('/api/public', require('./app/routes/public'))
//TODO add session
routes.use('/api/private', passport.authenticate('jwt', {session:false}), require('./app/routes/private'));

routes.use((req, res, next)=> {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin',  allowedURL + port);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Pass to next layer of middleware
    next();
})
// Server index.html page when request to the root is made
routes.get('/', function (req, res, next) {
    res.sendfile('./public/index.html');
})
