const routes = require('express')();

//registers our authentication routes with Express.
routes.use('/organisation', require('./controllers/organisation'));

module.exports = routes;
