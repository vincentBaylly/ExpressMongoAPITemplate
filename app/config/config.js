module.exports = {
  DB_URL: process.env.DB_URL  || 'localhost',
  ALLOWED_URL: process.env.ALLOWED_URL,
  APP_PORT: process.env.APP_PORT || 4000,
  DB_PORT: process.env.DB_PORT || 27017,
  DB_USER: process.env.DB_USER || 'adminMongo',
  DB_PWD: process.env.DB_PWD || '@dm1nMongo',
  DB_COLLECTION: process.env.DB_COLLECTION || 'database'
}
