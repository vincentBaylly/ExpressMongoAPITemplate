const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const OrganisationSchema = new Schema({
  name: {
    type: String
  }
}
)

module.exports = Organisation = mongoose.model('Organisation', OrganisationSchema);
